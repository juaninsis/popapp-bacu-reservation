build:
	docker build --no-cache -t popapp-bacu-app -f ./Dockerfile.test .

run:
	docker run -dp 3000:3000 -i popapp-bacu-app

deploy: build run

up:
	docker-compose up --build

generate-doc: generate-swagger rename-swagger-json rename-swagger-yml

generate-swagger:
	swag init -g ./cmd/server.go --output docs --parseDependency

rename-swagger-json:
	mv docs/swagger.json docs/popapp-bacu-reservation.json

rename-swagger-yml:
	mv docs/swagger.yaml docs/popapp-bacu-reservation.yml
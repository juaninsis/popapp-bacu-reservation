package main

import (
	"fmt"

	"github.com/labstack/echo/v4"
	echoSwagger "github.com/swaggo/echo-swagger"
	"github.com/swaggo/swag"
	"gitlab.com/popapp-bacu-reservation/docs"
	"gitlab.com/popapp-bacu-reservation/internal/health"
	"gitlab.com/popapp-bacu-reservation/internal/reservation"
)

const (
	popappBacuReservation = "popapp-bacu-reservation"
)

func main() {
	fmt.Println("The Reservation server started!")
	BuildServer()
}

func BuildServer() {
	e := buildServer()
	routes := e.Group("")
	serveDocs(routes)
	health.Serve(routes)
	reservation.Serve(routes)
	e.Logger.Fatal(e.Start(":" + "3000"))
}
func buildServer() *echo.Echo {
	e := echo.New()
	return e
}

func serveDocs(routeGroups *echo.Group) {
	docs.SwaggerInfo.InfoInstanceName = popappBacuReservation
	swag.Register(docs.SwaggerInfo.InstanceName(), docs.SwaggerInfo)
	routeGroups.GET("/docs/*", echoSwagger.EchoWrapHandler(
		func(c *echoSwagger.Config) { c.InstanceName = popappBacuReservation }),
	)
}

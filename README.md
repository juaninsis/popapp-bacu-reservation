# popapp-bacu-reservation



## Objetivo

Construir un API que permita reservar un número de 1 a n máximo una vez por cliente,
cada cliente puede reservar máximo un número. En cualquier momento, también se debe
poder consultar por un API REST la lista de número que ha sido reservado con su
respectivo identificador de cliente.

Una vez un número es reservado, no se debe poder reservar de nuevo, y el servidor debe
arrojar un error adecuado indicando el motivo del error.

## Como ejecutarlo?

El proyecto se puede ejecutar en k8s usando el Dockerfile o de forma local usando el Dockerfile.test + docker-compose

Todos los comandos se encuentran en el Makefile. Para ejecutar el proyecto de forma local, correr el commando:

```
make up
```

## Doc

La documentación se puede consultar en <uri>/docs/index.html

En el caso de ejecutar el proyecto de forma local http://localhost:3000/docs/index.html


## CURL examples

### Crear reserva

```
curl --location --request POST 'http://localhost:3000/reserve' \
--header 'Content-Type: application/json' \
--data-raw '{
    "client_id":"client1",
    "reserva": 2
}'
```

Respuesta 200

```
{
    "client_id": "client1",
    "reserva": 2,
    "status": "OK"
}
```


### Consultar reservas

```
curl --location --request GET 'http://localhost:3000/reservations'
```

Respuesta 200

```
{
    "reservations": [
        {
            "client_id": "client1",
            "reserva": 2
        },
        {
            "client_id": "client2",
            "reserva": 4
        }
    ]
}
```
package health

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func Serve(routeGroups *echo.Group) {
	routeGroups.GET("/health", healthCheck)
}

// HealthCheck godoc
// @Summary Show the status of server.
// @Description Get the status of server.
// @Tags healthCheck
// @Accept */*
// @Produce json
// @Success 200 {object} string
// @Router /health [get]
func healthCheck(c echo.Context) error {
	return c.JSON(http.StatusOK, "ok")
}

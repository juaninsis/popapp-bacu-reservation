package cfg

import "github.com/joeshaw/envdecode"

var (
	config Config
)

type (
	Config struct {
		RedisHost string `env:"REDIS_HOST,default=redis"`
		RedisPort string `env:"REDIS_PORT,default=6379"`
	}
)

func initCfg() {
	if err := envdecode.Decode(&config); err != nil {
		panic(err)
	}
}

func GetConfig() Config {
	initCfg()
	return config
}

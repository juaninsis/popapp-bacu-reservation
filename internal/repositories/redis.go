package repositories

import (
	"context"
	"encoding/json"
	"time"

	"github.com/go-redis/redis/v8"
	log "github.com/sirupsen/logrus"
)

type (
	redisClient struct {
		client *redis.Client
	}

	Redis interface {
		Set(ctx context.Context, key string, value interface{}, expire time.Duration)
		Get(ctx context.Context, key string) ([]byte, error)
	}
)

func NewRedis(client *redis.Client) Redis {
	return &redisClient{
		client: client,
	}
}

func (rc *redisClient) Set(ctx context.Context, key string, value interface{}, expire time.Duration) {
	jsonStr, err := json.Marshal(value)
	if err != nil {
		log.Errorf("marshal error: %s", err.Error())
		return
	}

	if err := rc.client.Set(context.Background(), key, jsonStr, expire).Err(); err != nil {
		log.Errorf("redis set key %s, error: %s", key, err.Error())
	}
}

func (rc *redisClient) Get(ctx context.Context, key string) ([]byte, error) {
	val, err := rc.client.Get(ctx, key).Result()
	if err != nil {
		return nil, err
	}

	return []byte(val), nil
}

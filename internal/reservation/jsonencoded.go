package reservation

type (
	NumResponse struct {
		ClientID string  `json:"client_id"`
		Num      int64   `json:"reserva"`
		Status   *string `json:"status,omitempty"`
	}

	Request struct {
		ClientID string `json:"client_id"`
		Num      int64  `json:"reserva"`
	}

	ReservationResponse struct {
		Reservations []NumResponse `json:"reservations"`
	}

	Error struct {
		Message string `json:"message"`
	}
)

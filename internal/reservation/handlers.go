package reservation

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"
)

var (
	ok = "OK"
)

func Serve(routeGroups *echo.Group) {
	routeGroups.POST("/reserve", handlerSave())
	routeGroups.GET("/reservations", handlerGet())
}

// Reservation godoc
// @Summary Post Reservation.
// @Description Set Reservation information
// @Tags Reservation
// @Accept json
// @Param reservation body Request false "Reservation"
// @Produce json
// @Success 200  {object} NumResponse
// @Failure 400  {object} Error
// @Router /reserve [POST]
func handlerSave() echo.HandlerFunc {
	return func(c echo.Context) error {
		var (
			err  error
			logH = log.WithField("function", "handler_save")
		)

		req := Request{}
		d := json.NewDecoder(c.Request().Body)
		d.DisallowUnknownFields()
		if err = d.Decode(&req); err != nil {
			return getHTTPError(http.StatusBadRequest, err)
		}
		if req.ClientID == "" {
			err = errors.New("client_id is required")
			logH.Error(err)
			return getHTTPError(http.StatusBadRequest, err)
		}
		if req.Num == 0 {
			err = errors.New("reserva is required")
			logH.Error(err)
			return getHTTPError(http.StatusBadRequest, err)
		}

		if err = setReservation(c, req); err != nil {
			logH.Error(err)
			return getHTTPError(http.StatusBadRequest, err)
		}
		res := NumResponse{
			ClientID: req.ClientID,
			Num:      req.Num,
			Status:   &ok,
		}
		return c.JSON(http.StatusOK, res)
	}
}

// Reservation godoc
// @Summary Get Reservation.
// @Description Get Reservations
// @Tags Reservation
// @Accept json
// @Produce json
// @Success 200	 {object} ReservationResponse
// @Failure 404  {object} Error
// @Router /reservations [GET]
func handlerGet() echo.HandlerFunc {
	return func(c echo.Context) error {
		var (
			err  error
			res  ReservationResponse
			logH = log.WithField("function", "handler_get")
		)
		if res, err = getReservations(c); err != nil {
			logH.Error(err)
			err = errors.New("not found")
			return getHTTPError(http.StatusNotFound, err)
		}
		return c.JSON(http.StatusOK, res)
	}
}

func getHTTPError(code int, err error) error {
	return echo.NewHTTPError(code, Error{
		Message: err.Error(),
	})
}

package reservation

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/go-redis/redis/v8"
	"github.com/labstack/echo/v4"
	"gitlab.com/popapp-bacu-reservation/internal/cfg"
	"gitlab.com/popapp-bacu-reservation/internal/repositories"
)

const (
	numKey       = "reservation-num-%d"
	clientIDKey  = "reservation-cli-%s"
	reservations = "reservations"
)

var (
	conf     = cfg.GetConfig()
	redisCli = repositories.NewRedis(
		redis.NewClient(&redis.Options{
			Addr:     fmt.Sprintf("%s:%s", conf.RedisHost, conf.RedisPort),
			Password: "",
			DB:       0,
		}))
)

func setReservation(c echo.Context, req Request) error {
	ctx := c.Request().Context()

	//validate client
	kC := fmt.Sprintf(clientIDKey, req.ClientID)
	if exists(ctx, req.Num, kC) {
		return errors.New(fmt.Sprintf("%s ya tiene reserva", req.ClientID))
	}

	//validate reserva
	kN := fmt.Sprintf(numKey, req.Num)
	if exists(ctx, req.Num, kN) {
		return errors.New("Número ya reservado")
	}
	r, err := getReservations(c)
	if err != nil || r.Reservations == nil {
		r.Reservations = make([]NumResponse, 0)
	}
	r.Reservations = append(r.Reservations, NumResponse{
		ClientID: req.ClientID,
		Num:      req.Num,
	})

	//reserve
	go redisCli.Set(ctx, reservations, r, 0)
	go redisCli.Set(ctx, kN, req.ClientID, 0)
	go redisCli.Set(ctx, kC, req.Num, 0)

	return nil
}

func getReservations(c echo.Context) (ReservationResponse, error) {
	ctx := c.Request().Context()
	b, err := redisCli.Get(ctx, reservations)
	if err != nil {
		return ReservationResponse{}, err
	}

	res := ReservationResponse{}
	if err = json.Unmarshal(b, &res); err != nil {
		return ReservationResponse{}, err
	}
	return res, nil
}

func exists(ctx context.Context, num int64, k string) bool {
	b, err := redisCli.Get(ctx, k)
	if err == nil && b != nil {
		return true
	}
	return false
}
